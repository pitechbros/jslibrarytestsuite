

const site = 'https://www.happymarketer.com/';
let aboutUS = '#nav-menu-item-8494 > a';
let CaseStudies = '#nav-menu-item-14992 > a'
let training = '#nav-menu-item-8497> a';
let img = 'img.mobile';


function set1(browser) {

  browser.click(img).click(aboutUS)
          .click(aboutUS).click(aboutUS)
          .click(CaseStudies).click(img).click(aboutUS)
          .click(training).click(aboutUS).click(training)
          .click(training).click(aboutUS).click(img)
          .click(CaseStudies).click(img);
}

function set2(browser) {
  browser
         .click(img).click(img).click(img).click(img).click(img)
         .click(aboutUS).click(aboutUS).click(aboutUS).click(aboutUS).click(aboutUS)
         .click(training).click(training).click(training).click(training).click(training)
         .click(CaseStudies).click(CaseStudies).click(CaseStudies).click(CaseStudies).click(CaseStudies);
}


module.exports = {
    before : function(browser) {
      browser.globals.waitForConditionTimeout = 5000
      browser
      .url(site)
      .waitForElementVisible('body', 1000)
    },
    after: function(browser) {
      browser.end();
    },
    'Set1: 30 Events Per browser: Total Browsers = 2; First page load event per browser: 1: Total events sent 62': function(browser) {
       set1(browser);
    },
    'Set2: 40 similar Events Per Browser: Total events sent 80': function(browser) {
       set2(browser);
       console.log("Total PageViews", 72);
       console.log("Total Events Sent including PageViews", 142);
    }
}