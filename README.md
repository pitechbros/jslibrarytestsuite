# Event360 TestSuite 


### What is this repository for? ###

* Testing the number of events sent by client library and matching it with BQ data
* 1.0


### How do I get set up? ###


* Install packages
	```
	npm install
	```
* How to run tests
	```
	npm test
	```

